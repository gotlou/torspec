# Proposals for changes in the Tor protocols

This "book" is a list of proposals that people have made over the
years, (dating back to 2007) for protocol changes in Tor.
Some of these proposals are already implemented or rejected;
others are under active discussion.

If you're looking for a specific proposal, you can find it,
by filename, in the summary bar on the left, or at
[this index](./BY_INDEX.md).  You can also see a list of Tor protocols
by their status at [`README.md`].

For information on creating a new proposal, you would ideally look at
[`001-process.txt`].  That file is a bit out-of-date, though, and you
should probably just contact the developers.

* <a href="..">Back to the Tor specifications</a>
