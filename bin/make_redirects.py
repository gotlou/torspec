#!/usr/bin/env python3

import yaml, os, re, sys

def update_file(fname, start_marker, end_marker, replacement):
    content = list(open(fname, 'r').readlines())
    startline = content.index(start_marker)
    endline = content.index(end_marker)
    assert endline > startline
    content[startline+1:endline] = replacement

    with open(fname+".tmp", 'w') as f:
        f.write("".join(content))
    os.rename(fname+".tmp", fname)

BOOK_START = "# BEGIN AUTO-GENERATED REDIRECTS\n"
BOOK_END = "# END AUTO-GENERATED REDIRECTS\n"

README_START = "<!-- BEGIN AUTO-GENERATED REDIRECTS -->\n"
README_END = "<!-- END AUTO-GENERATED REDIRECTS -->\n"

def book_redirects(rs, spec_dir):
    lines = []
    for kwd, info in rs.items():
        if os.path.isdir(os.path.join(spec_dir, kwd)):
            assert os.path.isfile(os.path.join(spec_dir, kwd, "index.md"))
            assert info.get('implicit')
            continue
        elif any((os.path.isfile(os.path.join(spec_dir, kwd) + ext)) for ext in [".txt", ".md"]):
            assert info.get('implicit')
            continue
        else:
            assert not info.get('implicit')
            source = kwd + ".html"
        target = info['target']
        lines.append(
            f'"/{source}" = "{target}"\n'
        )
    return "".join(lines)

def readme_redirects(rs):
    lines = [ "<dl>\n" ]
    for kwd, info in rs.items():
        target = info['target']
        desc = info['description']
        lines.append(f'<dt><a href="/{kwd}"><code>/{kwd}</code></a></dt>\n')
        lines.append(f'<dd><a href="{target}"><code>{target}</code> ({desc})</a></dt>\n')

    lines.append("</dl>\n")
    return "".join(lines)

def proposal_redirects(proposals_dir):
    lines = []
    for fname in os.listdir(proposals_dir):
        m = re.match(r'^(\d+)-.*\.(?:md|txt)$', fname)
        if m:
            source = m.group(1) + ".html"
            target, targetext = os.path.splitext(fname)
            if targetext == '.md':
                targetext = ".html"
            lines.append(f'"/{source}" = "./{target}{targetext}"\n')
    lines.sort()
    return "".join(lines)

if __name__ == '__main__':
    toplevel = os.path.join(os.path.dirname(sys.argv[0]), "..")
    spec_book_fname = os.path.join(toplevel, "mdbook", "spec", "book.toml")
    spec_dir = os.path.join(toplevel, "spec")
    readme_fname = os.path.join(toplevel, "spec", "README.md")
    prop_dir = os.path.join(toplevel, "proposals")
    prop_book_fname = os.path.join(toplevel, "mdbook", "proposals", "book.toml")
    yaml_fname = os.path.join(toplevel, "mdbook", "spec", "spec-redirects.yaml")

    rs = yaml.load(open(yaml_fname), yaml.Loader)['redirects']

    update_file(spec_book_fname, BOOK_START, BOOK_END, book_redirects(rs, spec_dir))
    update_file(readme_fname, README_START, README_END, readme_redirects(rs))
    update_file(prop_book_fname, BOOK_START, BOOK_END, proposal_redirects(prop_dir))
