#!/usr/bin/env bash

set -e -u -o pipefail -x

TOPLEVEL=$(realpath $(dirname "$0"))/..
cd "${TOPLEVEL}"
./bin/reindex.py

./bin/make_redirects.py

cd "${TOPLEVEL}/mdbook/spec"
mdbook build

cd "${TOPLEVEL}/mdbook/proposals"
mdbook build
