<a id="tor-spec.txt-5.3"></a>

# Creating circuits

When creating a circuit through the network, the circuit creator
(OP) performs the following steps:

```text
      1. Choose an onion router as an end node (R_N):
         * N MAY be 1 for non-anonymous directory mirror, introduction point,
           or service rendezvous connections.
         * N SHOULD be 3 or more for anonymous connections.
         Some end nodes accept streams (see 6.1), others are introduction
         or rendezvous points (see rend-spec-{v2,v3}.txt).

      2. Choose a chain of (N-1) onion routers (R_1...R_N-1) to constitute
         the path, such that no router appears in the path twice.

      3. If not already connected to the first router in the chain,
         open a new connection to that router.

      4. Choose a circID not already in use on the connection with the
         first router in the chain; send a CREATE/CREATE2 cell along
         the connection, to be received by the first onion router.

      5. Wait until a CREATED/CREATED2 cell is received; finish the
         handshake and extract the forward key Kf_1 and the backward
         key Kb_1.

      6. For each subsequent onion router R (R_2 through R_N), extend
         the circuit to R.
```

To extend the circuit by a single onion router R_M, the OP performs
these steps:

1. Create an onion skin, encrypted to R_M's public onion key.

```text
      2. Send the onion skin in a relay EXTEND/EXTEND2 cell along
         the circuit (see sections 5.1.2 and 5.5).

      3. When a relay EXTENDED/EXTENDED2 cell is received, verify KH,
         and calculate the shared keys.  The circuit is now extended.
```

When an onion router receives an EXTEND relay cell, it sends a CREATE
cell to the next onion router, with the enclosed onion skin as its
payload.

When an onion router receives an EXTEND2 relay cell, it sends a CREATE2
cell to the next onion router, with the enclosed HLEN, HTYPE, and HDATA
as its payload. The initiating onion router chooses some circID not yet
used on the connection between the two onion routers. (But see section
5.1.1 above, concerning choosing circIDs.)

As special cases, if the EXTEND/EXTEND2 cell includes a legacy identity, or
identity fingerprint of all zeroes, or asks to extend back to the relay
that sent the extend cell, the circuit will fail and be torn down.

Ed25519 identity keys are not required in EXTEND2 cells, so all zero
keys SHOULD be accepted. If the extending relay knows the ed25519 key from
the consensus, it SHOULD also check that key. (See section 5.1.2.)

If an EXTEND2 cell contains the ed25519 key of the relay that sent the
extend cell, the circuit will fail and be torn down.

When an onion router receives a CREATE/CREATE2 cell, if it already has a
circuit on the given connection with the given circID, it drops the
cell. Otherwise, after receiving the CREATE/CREATE2 cell, it completes
the specified handshake, and replies with a CREATED/CREATED2 cell.

Upon receiving a CREATED/CREATED2 cell, an onion router packs it payload
into an EXTENDED/EXTENDED2 relay cell (see section 5.1.2), and sends
that cell up the circuit. Upon receiving the EXTENDED/EXTENDED2 relay
cell, the OP can retrieve the handshake material.

(As an optimization, OR implementations may delay processing onions
until a break in traffic allows time to do so without harming
network latency too greatly.)

<a id="tor-spec.txt-5.3.1"></a>

## Canonical connections

It is possible for an attacker to launch a man-in-the-middle attack
against a connection by telling OR Alice to extend to OR Bob at some
address X controlled by the attacker.  The attacker cannot read the
encrypted traffic, but the attacker is now in a position to count all
bytes sent between Alice and Bob (assuming Alice was not already
connected to Bob.)

To prevent this, when an OR gets an extend request, it SHOULD use an
existing OR connection if the ID matches, and ANY of the following
conditions hold:

```text
       - The IP matches the requested IP.
       - The OR knows that the IP of the connection it's using is canonical
         because it was listed in the NETINFO cell.

    ORs SHOULD NOT check the IPs that are listed in the server descriptor.
    Trusting server IPs makes it easier to covertly impersonate a relay, after
    stealing its keys.
```
