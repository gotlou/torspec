<a id="tor-spec.txt-2"></a>

# Connections

Connections between two Tor relays, or between a client and a relay,
use TLS/SSLv3 for link authentication and encryption.  All
implementations MUST support the SSLv3 ciphersuite
"TLS_DHE_RSA_WITH_AES_128_CBC_SHA" if it is available. They SHOULD
support better ciphersuites if available.

There are three ways to perform TLS handshakes with a Tor server.  In
the first way, "certificates-up-front", both the initiator and
responder send a two-certificate chain as part of their initial
handshake.  (This is supported in all Tor versions.)  In the second
way, "renegotiation", the responder provides a single certificate,
and the initiator immediately performs a TLS renegotiation.  (This is
supported in Tor 0.2.0.21 and later.)  And in the third way,
"in-protocol", the initial TLS negotiation completes, and the
parties bootstrap themselves to mutual authentication via use of the
Tor protocol without further TLS handshaking.  (This is supported in
0.2.3.6-alpha and later.)

Each of these options provides a way for the parties to learn it is
available: a client does not need to know the version of the Tor
server in order to connect to it properly.

In "certificates up-front" (a.k.a "the v1 handshake"),
the connection initiator always sends a
two-certificate chain, consisting of an X.509 certificate using a
short-term connection public key and a second, self-signed X.509
certificate containing its identity key.  The other party sends a similar
certificate chain.  The initiator's ClientHello MUST NOT include any
ciphersuites other than:

```text
     TLS_DHE_RSA_WITH_AES_256_CBC_SHA
     TLS_DHE_RSA_WITH_AES_128_CBC_SHA
     SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA
```

In "renegotiation" (a.k.a. "the v2 handshake"),
the connection initiator sends no certificates, and
the responder sends a single connection certificate.  Once the TLS
handshake is complete, the initiator renegotiates the handshake, with each
party sending a two-certificate chain as in "certificates up-front".
The initiator's ClientHello MUST include at least one ciphersuite not in
the list above -- that's how the initiator indicates that it can
handle this handshake.  For other considerations on the initiator's
ClientHello, see section 2.1 below.

In "in-protocol" (a.k.a. "the v3 handshake"), the initiator sends no
certificates, and the
responder sends a single connection certificate.  The choice of
ciphersuites must be as in a "renegotiation" handshake.  There are
additionally a set of constraints on the connection certificate,
which the initiator can use to learn that the in-protocol handshake
is in use.  Specifically, at least one of these properties must be
true of the certificate:

```text
      * The certificate is self-signed
      * Some component other than "commonName" is set in the subject or
        issuer DN of the certificate.
      * The commonName of the subject or issuer of the certificate ends
        with a suffix other than ".net".
      * The certificate's public key modulus is longer than 1024 bits.
```

The initiator then sends a VERSIONS cell to the responder, which then
replies with a VERSIONS cell; they have then negotiated a Tor
protocol version.  Assuming that the version they negotiate is 3 or higher
(the only ones specified for use with this handshake right now), the
responder sends a CERTS cell, an AUTH_CHALLENGE cell, and a NETINFO
cell to the initiator, which may send either CERTS, AUTHENTICATE,
NETINFO if it wants to authenticate, or just NETINFO if it does not.

For backward compatibility between later handshakes and "certificates
up-front", the ClientHello of an initiator that supports a later
handshake MUST include at least one ciphersuite other than those listed
above. The connection responder examines the initiator's ciphersuite list
to see whether it includes any ciphers other than those included in the
list above.  If extra ciphers are included, the responder proceeds as in
"renegotiation" and "in-protocol": it sends a single certificate and
does not request
client certificates.  Otherwise (in the case that no extra ciphersuites
are included in the ClientHello) the responder proceeds as in
"certificates up-front": it requests client certificates, and sends a
two-certificate chain.  In either case, once the responder has sent its
certificate or certificates, the initiator counts them.  If two
certificates have been sent, it proceeds as in "certificates up-front";
otherwise, it proceeds as in "renegotiation" or "in-protocol".

To decide whether to do "renegotiation" or "in-protocol", the
initiator checks whether the responder's initial certificate matches
the criteria listed above.

All new relay implementations of the Tor protocol MUST support
backwards-compatible renegotiation; clients SHOULD do this too.  If
this is not possible, new client implementations MUST support both
"renegotiation" and "in-protocol" and use the router's
published link protocols list (see dir-spec.txt on the "protocols" entry)
to decide which to use.

In all of the above handshake variants, certificates sent in the clear
SHOULD NOT include any strings to identify the host as a Tor relay. In
the "renegotiation" and "backwards-compatible renegotiation" steps, the
initiator SHOULD choose a list of ciphersuites and TLS extensions
to mimic one used by a popular web browser.

Even though the connection protocol is identical, we will think of the
initiator as either an onion router (OR) if it is willing to relay
traffic for other Tor users, or an onion proxy (OP) if it only handles
local requests. Onion proxies SHOULD NOT provide long-term-trackable
identifiers in their handshakes.

In all handshake variants, once all certificates are exchanged, all
parties receiving certificates must confirm that the identity key is as
expected.  If the key is not as expected, the party must close the
connection.

(When initiating a connection, if a reasonably live consensus is
available, then the expected identity key is taken from that
consensus. But when initiating a connection otherwise, the expected
identity key is the one given in the hard-coded authority or
fallback list.  Finally, when creating a connection because of an
EXTEND/EXTEND2 cell, the expected identity key is the one given in
the cell.)

When connecting to an OR, all parties SHOULD reject the connection if that
OR has a malformed or missing certificate.  When accepting an incoming
connection, an OR SHOULD NOT reject incoming connections from parties with
malformed or missing certificates.  (However, an OR should not believe
that an incoming connection is from another OR unless the certificates
are present and well-formed.)

\[Before version 0.1.2.8-rc, ORs rejected incoming connections from ORs and
OPs alike if their certificates were missing or malformed.\]

Once a TLS connection is established, the two sides send cells
(specified below) to one another.  Cells are sent serially.  Standard
cells are CELL_LEN(link_proto) bytes long, but variable-length cells
also exist; see Section 3.  Cells may be sent embedded in TLS records
of any size or divided across TLS records, but the framing of TLS
records MUST NOT leak information about the type or contents of the
cells.

TLS connections are not permanent. Either side MAY close a connection
if there are no circuits running over it and an amount of time
(KeepalivePeriod, defaults to 5 minutes) has passed since the last time
any traffic was transmitted over the TLS connection.  Clients SHOULD
also hold a TLS connection with no circuits open, if it is likely that a
circuit will be built soon using that connection.

Client-only Tor instances are encouraged to avoid using handshake
variants that include certificates, if those certificates provide
any persistent tags to the relays they contact. If clients do use
certificates, they SHOULD NOT keep using the same certificates when
their IP address changes.  Clients MAY send certificates using any
of the above handshake variants.

<a id="tor-spec.txt-2.1"></a>

## Picking TLS ciphersuites

Clients SHOULD send a ciphersuite list chosen to emulate some popular
web browser or other program common on the internet. Clients may send
the "Fixed Cipheruite List" below.  If they do not, they MUST NOT
advertise any ciphersuite that they cannot actually support, unless that
cipher is one not supported by OpenSSL 1.0.1.

The fixed ciphersuite list is:

```text
     TLS1_ECDHE_ECDSA_WITH_AES_256_CBC_SHA
     TLS1_ECDHE_RSA_WITH_AES_256_CBC_SHA
     TLS1_DHE_RSA_WITH_AES_256_SHA
     TLS1_DHE_DSS_WITH_AES_256_SHA
     TLS1_ECDH_RSA_WITH_AES_256_CBC_SHA
     TLS1_ECDH_ECDSA_WITH_AES_256_CBC_SHA
     TLS1_RSA_WITH_AES_256_SHA
     TLS1_ECDHE_ECDSA_WITH_RC4_128_SHA
     TLS1_ECDHE_ECDSA_WITH_AES_128_CBC_SHA
     TLS1_ECDHE_RSA_WITH_RC4_128_SHA
     TLS1_ECDHE_RSA_WITH_AES_128_CBC_SHA
     TLS1_DHE_RSA_WITH_AES_128_SHA
     TLS1_DHE_DSS_WITH_AES_128_SHA
     TLS1_ECDH_RSA_WITH_RC4_128_SHA
     TLS1_ECDH_RSA_WITH_AES_128_CBC_SHA
     TLS1_ECDH_ECDSA_WITH_RC4_128_SHA
     TLS1_ECDH_ECDSA_WITH_AES_128_CBC_SHA
     SSL3_RSA_RC4_128_MD5
     SSL3_RSA_RC4_128_SHA
     TLS1_RSA_WITH_AES_128_SHA
     TLS1_ECDHE_ECDSA_WITH_DES_192_CBC3_SHA
     TLS1_ECDHE_RSA_WITH_DES_192_CBC3_SHA
     SSL3_EDH_RSA_DES_192_CBC3_SHA
     SSL3_EDH_DSS_DES_192_CBC3_SHA
     TLS1_ECDH_RSA_WITH_DES_192_CBC3_SHA
     TLS1_ECDH_ECDSA_WITH_DES_192_CBC3_SHA
     SSL3_RSA_FIPS_WITH_3DES_EDE_CBC_SHA
     SSL3_RSA_DES_192_CBC3_SHA
     [*] The "extended renegotiation is supported" ciphersuite, 0x00ff, is
         not counted when checking the list of ciphersuites.
```

If the client sends the Fixed Ciphersuite List, the responder MUST NOT
select any ciphersuite besides TLS_DHE_RSA_WITH_AES_256_CBC_SHA,
TLS_DHE_RSA_WITH_AES_128_CBC_SHA, and SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA:
such ciphers might not actually be supported by the client.

If the client sends a v2+ ClientHello with a list of ciphers other then
the Fixed Ciphersuite List, the responder can trust that the client
supports every cipher advertised in that list, so long as that ciphersuite
is also supported by OpenSSL 1.0.1.

Responders MUST NOT select any TLS ciphersuite that lacks ephemeral keys,
or whose symmetric keys are less then KEY_LEN bits, or whose digests are
less than HASH_LEN bits.  Responders SHOULD NOT select any SSLv3
ciphersuite other than the DHE+3DES suites listed above.

<a id="tor-spec.txt-2.2"></a>

## TLS security considerations

Implementations MUST NOT allow TLS session resumption -- it can
exacerbate some attacks (e.g. the "Triple Handshake" attack from
Feb 2013), and it plays havoc with forward secrecy guarantees.

Implementations SHOULD NOT allow TLS compression -- although we don't
know a way to apply a CRIME-style attack to current Tor directly,
it's a waste of resources.
