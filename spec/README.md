# Tor specifications

These specifications describe how Tor works.  They try to present
Tor's protocols in sufficient detail to allow the reader to implement
a compatible implementation of Tor without ever having to read the Tor
source code.

They were once a separate set of text files, but in late 2023 we
migrated to use [`mdbook`](https://rust-lang.github.io/mdBook/).
We're in the process of updating these documents to improve their quality.

This is a living document: we are always changing and improving them
in order to make them easier and more accurate, and to improve the
quality of the Tor protocols.  They are maintained as a set of
documents in a
[gitlab repository](https://gitlab.torproject.org/tpo/core/torspec/);
you can use that repository to see their history.

Additionally, the <a href="./proposals/index.html">proposals</a>
directory holds our design proposals.  These include historical
documents that have now been merged into the main specifications, and
new proposals that are still under discussion.  Of particular
interrest are the
<a href="./proposals/BY_STATUS.html#finished-proposals-implemented-specs-not-merged">`FINISHED`
Tor proposals</a>: They are the ones that have been implemented, but
not yet merged into these documents.

## Getting started

There's a lot of material here, and it's not always as well organized
as we like.  We have broken it into a few major sections.

For a table of contents, click on the menu icon to the top-left of
your browser scene.  You should probably start by reading [the core
tor protocol specification](./tor-spec), which describes how our
protocol works.  After that, you should be able to jump around to
the topics that interest you most.  The introduction of each top-level
chapter should provide an introduction.

## How to edit these specifications

These specifications are displayed using a tool called
[`mdbook`](https://rust-lang.github.io/mdBook/); we recommend reading
its documentation.

To edit these specs, clone the [git repository] and edit the
appropriate file in the [`spec` directory].  These files will match
the URLs of their corresponding pages, so if you want to edit
[`tor-spec/flow-control.html`](./tor-spec/flow-control),
you'll be looking for a file
called [`spec/tor-spec/flow-control.md`].

The chapter structure as a whole is defined in a special file called
[`SUMMARY.md`].  Its format is pretty restrictive; see the [mdbook
documentation][mdbook documentation] for more information.

You can build the website locally to see how it renders.  To do this,
just install mdbook and run the `./bin/build_html.sh` script.  (You will
need to have mdbook installed to do this.)

We have started a [style guide](./STYLE.md) for writing new parts of
this spec; as of 2023 it is quite preliminary. You should feel free to
edit it!

### Managing links

We're in the early stages of this spec organization, but we should
still be thinking about long term maintainability.

Please think about how to keep links working in the long term.
If you are going to add a link to a file, make sure that the file's
name is reasonable.  Before you rename a file, consider adding
a redirect from the file's old name.  (See the mdbook documentation
for more information about how.)

If you want to link to a specific section within a file,
make sure that the section has a defined anchor that makes sense.
The syntax to define [heading ids] in mdbook looks like this:

`## Heading with a long title that you want shorter name for { #shortname }`

If you need to change a heading, make sure that you keep its
`id` the same as it was before, so that links will still work.

Finally, when you're looking for specific sections (e.g., to fix
references that say "See section 5.2.3") you can look for the HTML
anchors that our conversion process added.  For example,
if you want to find `dir-spec.txt` section 2.1.3, look for
the anchor that says `<a id="dir-spec.txt-2.1.3"></a>`.

______________________________________________________________________

## Permalinks

Additionally, these URLs at `spec.toprorject.org` are intended to be
long-term permalinks.

<!-- BEGIN AUTO-GENERATED REDIRECTS -->
<dl>
<dt><a href="/address-spec"><code>/address-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/address-spec"><code>https://spec.torproject.org/address-spec</code> (Special Hostnames in Tor)</a></dt>
<dt><a href="/bandwidth-file-spec"><code>/bandwidth-file-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/bandwidth-file-spec"><code>https://spec.torproject.org/bandwidth-file-spec</code> (Directory Authority Bandwidth File spec)</a></dt>
<dt><a href="/bridgedb-spec"><code>/bridgedb-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/bridgedb-spec"><code>https://spec.torproject.org/bridgedb-spec</code> (BridgeDB specification)</a></dt>
<dt><a href="/cert-spec"><code>/cert-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/cert-spec"><code>https://spec.torproject.org/cert-spec</code> (Ed25519 certificates in Tor)</a></dt>
<dt><a href="/collector-protocol"><code>/collector-protocol</code></a></dt>
<dd><a href="https://gitlab.torproject.org/tpo/network-health/metrics/collector/-/blob/master/src/main/resources/docs/PROTOCOL?ref_type=heads"><code>https://gitlab.torproject.org/tpo/network-health/metrics/collector/-/blob/master/src/main/resources/docs/PROTOCOL?ref_type=heads</code> (Protocol of CollecTor's File Structure)</a></dt>
<dt><a href="/control-spec"><code>/control-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/control-spec"><code>https://spec.torproject.org/control-spec</code> (Tor control protocol, version 1)</a></dt>
<dt><a href="/dir-spec"><code>/dir-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/dir-spec"><code>https://spec.torproject.org/dir-spec</code> (Tor directory protocol, version 3)</a></dt>
<dt><a href="/dir-list-spec"><code>/dir-list-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/dir-list-spec"><code>https://spec.torproject.org/dir-list-spec</code> (Tor Directory List file format)</a></dt>
<dt><a href="/ext-orport-spec"><code>/ext-orport-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/ext-orport-spec"><code>https://spec.torproject.org/ext-orport-spec</code> (Extended ORPort for pluggable transports)</a></dt>
<dt><a href="/gettor-spec"><code>/gettor-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/gettor-spec"><code>https://spec.torproject.org/gettor-spec</code> (GetTor specification)</a></dt>
<dt><a href="/padding-spec"><code>/padding-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/padding-spec"><code>https://spec.torproject.org/padding-spec</code> (Tor Padding Specification)</a></dt>
<dt><a href="/path-spec"><code>/path-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/path-spec"><code>https://spec.torproject.org/path-spec</code> (Tor Path Specification)</a></dt>
<dt><a href="/pt-spec"><code>/pt-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/pt-spec"><code>https://spec.torproject.org/pt-spec</code> (Tor Pluggable Transport Specification, version 1)</a></dt>
<dt><a href="/rend-spec"><code>/rend-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/rend-spec"><code>https://spec.torproject.org/rend-spec</code> (Tor Onion Service Rendezvous Specification, latest version)</a></dt>
<dt><a href="/rend-spec-v2"><code>/rend-spec-v2</code></a></dt>
<dd><a href="https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/attic/rend-spec-v2.txt?ref_type=heads"><code>https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/attic/rend-spec-v2.txt?ref_type=heads</code> (Tor Onion Service Rendezvous Specification, Version 2 (Obsolete))</a></dt>
<dt><a href="/rend-spec-v3"><code>/rend-spec-v3</code></a></dt>
<dd><a href="https://spec.torproject.org/rend-spec"><code>https://spec.torproject.org/rend-spec</code> (Tor Onion Service Rendezvous Specification, Version 3 (Latest))</a></dt>
<dt><a href="/socks-extensions"><code>/socks-extensions</code></a></dt>
<dd><a href="https://spec.torproject.org/socks-extensions"><code>https://spec.torproject.org/socks-extensions</code> (Tor's extensions to the SOCKS protocol)</a></dt>
<dt><a href="/srv-spec"><code>/srv-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/srv-spec"><code>https://spec.torproject.org/srv-spec</code> (Tor Shared Random Subsystem Specification)</a></dt>
<dt><a href="/tor-fw-helper-spec"><code>/tor-fw-helper-spec</code></a></dt>
<dd><a href="https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/attic/tor-fw-helper-spec.txt?ref_type=heads"><code>https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/attic/tor-fw-helper-spec.txt?ref_type=heads</code> (Tor's (little) Firewall Helper specification)</a></dt>
<dt><a href="/tor-spec"><code>/tor-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/tor-spec"><code>https://spec.torproject.org/tor-spec</code> (Tor Protocol Specification)</a></dt>
<dt><a href="/torbrowser-design"><code>/torbrowser-design</code></a></dt>
<dd><a href="https://2019.www.torproject.org/projects/torbrowser/design/"><code>https://2019.www.torproject.org/projects/torbrowser/design/</code> (The Design and Implementation of the Tor Browser)</a></dt>
<dt><a href="/version-spec"><code>/version-spec</code></a></dt>
<dd><a href="https://spec.torproject.org/version-spec"><code>https://spec.torproject.org/version-spec</code> (How Tor Version Numbers Work)</a></dt>
<dt><a href="/tor-design"><code>/tor-design</code></a></dt>
<dd><a href="https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf"><code>https://svn.torproject.org/svn/projects/design-paper/tor-design.pdf</code> (Tor: The Second-Generation Onion Router)</a></dt>
<dt><a href="/walking-onions"><code>/walking-onions</code></a></dt>
<dd><a href="https://spec.torproject.org/proposals/323-walking-onions-full.html"><code>https://spec.torproject.org/proposals/323-walking-onions-full.html</code> (Walking Onions specifications)</a></dt>
</dl>
<!-- END AUTO-GENERATED REDIRECTS -->

[git repository]: https://gitlab.torproject.org/tpo/core/torspec/
[heading ids]: https://github.com/raphlinus/pulldown-cmark/blob/master/specs/heading_attrs.txt
[mdbook documentation]: https://rust-lang.github.io/mdBook/format/summary.html
[`spec/tor-spec/flow-control.md`]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/spec/tor-spec/flow-control.md?ref_type=heads
[`spec` directory]: https://gitlab.torproject.org/tpo/core/torspec/-/tree/main/spec?ref_type=heads
[`summary.md`]: https://gitlab.torproject.org/tpo/core/torspec/-/raw/main/spec/SUMMARY.md?ref_type=heads
