
# Managing streams

## Sending BEGIN messages { #send-begin }

In order to open a new stream to an onion service,
the client sends a BEGIN message on an established rendezvous circuit.

When sending a BEGIN message to an onion service,
a client should use an empty string as the target address,
and not set any flags on the begin message.

> For example, to open a connection to `<some_addr>.onion`
> on port 443, a client would send a BEGIN message with
> the address:port string of `":443"`, and a `FLAGS` value of 0.
> The 0-values `FLAGS` would not be encoded, according to
> the instructions for [encoding BEGIN messages](../tor-spec/opening/streams#opening).

## Receiving BEGIN messages { #receive-begin }

When a service receives a BEGIN message, it should check its port,
_and ignore all other fields in the begin message_, including its
address and flags.

If a service chooses to reject a BEGIN message, it should typically
destroy the circuit entirely to prevent port scanning,
resource exhaustion, and other undesirable behaviors.
If it does not, it should send back an `END` message with the `DONE` reason,
to avoid leaking any further information.


