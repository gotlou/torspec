# Tor specifications: style and usage notes


## Scope and intentions

These notes apply to our specifications.  When possible, they should
also apply to proposals, to make proposals easier to merge into our
specifications when they are done.

As of 2023, our existing specifications have been written without any
real style guidelines, so you should not expect to find these
guidelines to apply well to all of the documents as you read them.
Instead, these guidelines are for documentation going forward.

These notes are not terribly well organized.  We should improve them
over time. They are meant to be a living document.



## Other sources

There are a number of other style guides used in Tor.  We should
follow these as well.  If they do not suit our needs, we should try to
get them changed.

* [Community team guidelines](https://gitlab.torproject.org/tpo/community/team/-/issues/5)
* [Tor project glossary](https://support.torproject.org/glossary/)
* [Specifications glossary](./glossary.md)

(Please add more if you know about them!)

As we refine the guidelines in this file, we should attempt to get
them upstreamed to more project-wide guides, if they are suitable.

## Starting notes

Make sure you have read [this part of the README](https://spec.torproject.org/index.html#how-to-edit-these-specifications)!

## Vocabulary

We use these terms freely:
  * Channel
  * Circuit
  * Stream

We try not to say "connection" without qualification: There are too
many things in Tor that can be called a "connection".

Similarly, don't say "session" without qualification except when it is
clear from context what we mean.

Prefer "relay" to "node" or "server".

Prefer "service" and "client" when talking about onion services and
their users.

Refer to arti as arti and the C tor implementation as "the C tor
implementation" on first mention.  Subsequently you can call it `tor`
or "C tor".

Avoid "AP" and "OP" and "OR"; those are not in current usage.

## Documenting keys

TODO: Explain our new key documentation conventions, as used
[here](https://spec.torproject.org/rend-spec/protocol-overview.html#imd-key-menagerie).

## Documentating data encodings

We have two competing legacy schemes for documenting data encodings.
One of them is an ad-hoc format the looks like this:
```
   * FIELD_1     [4 bytes]
   * FIELD_2     [1 byte]
```

The other is a somewhat C-like format based on the
[`trunnel` format](https://gitlab.torproject.org/tpo/core/trunnel/-/blob/main/doc/trunnel.md?ref_type=heads).
It looks like this:
```
struct message {
   u32 field_1;
   u8 field_2;
}
```

Neither of these is really great.  We should find something better.

## Writing explanations

> When you are writing an explanation in the middle of a bunch of
> normative text, it is a good idea to put it in quoted text, like
> this.

