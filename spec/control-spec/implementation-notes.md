<a id="control-spec.txt-5"></a>

# Implementation notes

<a id="control-spec.txt-5.1"></a>

## Authentication

If the control port is open and no authentication operation is enabled, Tor
trusts any local user that connects to the control port.  This is generally
a poor idea.

If the 'CookieAuthentication' option is true, Tor writes a "magic
cookie" file named "control_auth_cookie" into its data directory (or
to another file specified in the 'CookieAuthFile' option).  To
authenticate, the controller must demonstrate that it can read the
contents of the cookie file:

- Current versions of Tor support cookie authentication

```text
    using the "COOKIE" authentication method: the controller sends the
    contents of the cookie file, encoded in hexadecimal.  This
    authentication method exposes the user running a controller to an
    unintended information disclosure attack whenever the controller
    has greater filesystem read access than the process that it has
    connected to.  (Note that a controller may connect to a process
    other than Tor.)  It is almost never safe to use, even if the
    controller's user has explicitly specified which filename to read
    an authentication cookie from.  For this reason, the COOKIE
    authentication method has been deprecated and will be removed from
    Tor before some future version of Tor.

  * 0.2.2.x versions of Tor starting with 0.2.2.36, and all versions of

    Tor after 0.2.3.12-alpha, support cookie authentication using the
    "SAFECOOKIE" authentication method, which discloses much less
    information about the contents of the cookie file.
```

If the 'HashedControlPassword' option is set, it must contain the salted
hash of a secret password.  The salted hash is computed according to the
S2K algorithm in RFC 2440 (OpenPGP), and prefixed with the s2k specifier.
This is then encoded in hexadecimal, prefixed by the indicator sequence
"16:".  Thus, for example, the password 'foo' could encode to:

```text
     16:660537E3E1CD49996044A3BF558097A981F539FEA2F9DA662B4626C1C2
        ++++++++++++++++**^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
           salt                       hashed value
                       indicator
```

You can generate the salt of a password by calling

```text
           'tor --hash-password <password>'
```

or by using the example code in the Python and Java controller libraries.
To authenticate under this scheme, the controller sends Tor the original
secret that was used to generate the password, either as a quoted string
or encoded in hexadecimal.

<a id="control-spec.txt-5.2"></a>

## Don't let the buffer get too big { #buffer-size }

With old versions of Tor (before 0.2.0.16-alpha), if you ask for
lots of events, and 16MB of them queue up on the buffer, the Tor
process will close the socket.

Newer Tor versions do not have this 16 MB buffer limit. However,
if you leave huge numbers of events unread, Tor may still run out
of memory, so you should still be careful about buffer size.

<a id="control-spec.txt-5.3"></a>

## Backward compatibility with v0 control protocol { #v0-compat }

The 'version 0' control protocol was replaced in Tor 0.1.1.x. Support
was removed in Tor 0.2.0.x. Every non-obsolete version of Tor now
supports the version 1 control protocol.

For backward compatibility with the "version 0" control protocol,
Tor used to check whether the third octet of the first command is zero.
(If it was, Tor assumed that version 0 is in use.)

This compatibility was removed in Tor 0.1.2.16 and 0.2.0.4-alpha.

<a id="control-spec.txt-5.4"></a>

## Tor config options for use by controllers { #special-config-options }

Tor provides a few special configuration options for use by controllers.
These options are not saved to disk by SAVECONF.  Most can be set and
examined by the SETCONF and GETCONF commands, but some (noted below) can
only be given in a torrc file or on the command line.

Generally, these options make Tor unusable by disabling a portion of Tor's
normal operations.  Unless a controller provides replacement functionality
to fill this gap, Tor will not correctly handle user requests.

\_\_AllDirActionsPrivate

```text
    If true, Tor will try to launch all directory operations through
    anonymous connections.  (Ordinarily, Tor only tries to anonymize
    requests related to hidden services.)  This option will slow down
    directory access, and may stop Tor from working entirely if it does not
    yet have enough directory information to build circuits.

    (Boolean. Default: "0".)

  __DisablePredictedCircuits

    If true, Tor will not launch preemptive "general-purpose" circuits for
    streams to attach to.  (It will still launch circuits for testing and
    for hidden services.)

    (Boolean. Default: "0".)

  __LeaveStreamsUnattached

    If true, Tor will not automatically attach new streams to circuits;
    instead, the controller must attach them with ATTACHSTREAM.  If the
    controller does not attach the streams, their data will never be routed.

    (Boolean. Default: "0".)

  __HashedControlSessionPassword

    As HashedControlPassword, but is not saved to the torrc file by
    SAVECONF.  Added in Tor 0.2.0.20-rc.

  __ReloadTorrcOnSIGHUP

    If this option is true (the default), we reload the torrc from disk
    every time we get a SIGHUP (from the controller or via a signal).
    Otherwise, we don't.  This option exists so that controllers can keep
    their options from getting overwritten when a user sends Tor a HUP for
    some other reason (for example, to rotate the logs).

    (Boolean.  Default: "1")

  __OwningControllerProcess

    If this option is set to a process ID, Tor will periodically check
    whether a process with the specified PID exists, and exit if one
    does not.  Added in Tor 0.2.2.28-beta.  This option's intended use
    is documented in section 3.23 with the related TAKEOWNERSHIP
    command.

    Note that this option can only specify a single process ID, unlike
    the TAKEOWNERSHIP command which can be sent along multiple control
    connections.

    (String.  Default: unset.)

  __OwningControllerFD

    If this option is a valid socket, Tor will start with an open control
    connection on this socket.  Added in Tor 0.3.3.1-alpha.

    This socket will be an owning controller, as if it had already called
    TAKEOWNERSHIP.  It will be automatically authenticated.  This option
    should only be used by other programs that are starting Tor.

    This option cannot be changed via SETCONF; it must be set in a torrc or
    via the command line.

    (Integer. Default: -1.)

  __DisableSignalHandlers

    If this option is set to true during startup, then Tor will not install
    any signal handlers to watch for POSIX signals.  The SIGNAL controller
    command will still work.

    This option is meant for embedding Tor inside another process, when
    the controlling process would rather handle signals on its own.

    This option cannot be changed via SETCONF; it must be set in a torrc or
    via the command line.

    (Boolean. Default: 0.)
```

<a id="control-spec.txt-5.5"></a>

## Phases from the Bootstrap status event { #bootstrap-phases }

```text
 [For the bootstrap phases reported by Tor prior to 0.4.0.x, see
  Section 5.6.]
```

This section describes the various bootstrap phases currently reported
by Tor. Controllers should not assume that the percentages and tags
listed here will continue to match up, or even that the tags will stay
in the same order. Some phases might also be skipped (not reported)
if the associated bootstrap step is already complete, or if the phase
no longer is necessary. Only "starting" and "done" are guaranteed to
exist in all future versions.

Current Tor versions enter these phases in order, monotonically.
Future Tors MAY revisit earlier phases, for example, if the network
fails.

<a id="control-spec.txt-5.5.1"></a>

### Overview of Bootstrap reporting { #bootstrap-overview }

Bootstrap phases can be viewed as belonging to one of three stages:

1. Initial connection to a Tor relay or bridge
1. Obtaining directory information
1. Building an application circuit

Tor doesn't specifically enter Stage 1; that is a side effect of
other actions that Tor is taking. Tor could be making a connection
to a fallback directory server, or it could be making a connection
to a guard candidate. Either one counts as Stage 1 for the purposes
of bootstrap reporting.

Stage 2 might involve Tor contacting directory servers, or it might
involve reading cached directory information from a previous
session. Large parts of Stage 2 might be skipped if there is already
enough cached directory information to build circuits. Tor will
defer reporting progress in Stage 2 until Stage 1 is complete.

Tor defers this reporting because Tor can already have enough
directory information to build circuits, yet not be able to connect
to a relay. Without that deferral, a user might misleadingly see Tor
stuck at a large amount of progress when something as fundamental as
making a TCP connection to any relay is failing.

Tor also doesn't specifically enter Stage 3; that is a side effect
of Tor building circuits for some purpose or other. In a typical
client, Tor builds predicted circuits to provide lower latency for
application connection requests. In Stage 3, Tor might make new
connections to relays or bridges that it did not connect to in Stage
1\.

<a id="control-spec.txt-5.5.2"></a>

### Phases in Bootstrap Stage 1  { #bootstrap-stage1 }

Phase 0:
tag=starting summary="Starting"

Tor starts out in this phase.

```text
  Phase 1:
  tag=conn_pt summary="Connecting to pluggable transport"
 [This phase is new in 0.4.0.x]
```

Tor is making a TCP connection to the transport plugin for a
pluggable transport. Tor will use this pluggable transport to make
its first connection to a bridge.

```text
  Phase 2:
  tag=conn_done_pt summary="Connected to pluggable transport"
 [New in 0.4.0.x]
```

Tor has completed its TCP connection to the transport plugin for the
pluggable transport.

```text
  Phase 3:
  tag=conn_proxy summary="Connecting to proxy"
 [New in 0.4.0.x]
```

Tor is making a TCP connection to a proxy to make its first
connection to a relay or bridge.

```text
  Phase 4:
  tag=conn_done_proxy summary="Connected to proxy"
 [New in 0.4.0.x]
```

Tor has completed its TCP connection to a proxy to make its first
connection to a relay or bridge.

```text
  Phase 5:
  tag=conn summary="Connecting to a relay"
 [New in 0.4.0.x; prior versions of Tor had a "conn_dir" phase that
  sometimes but not always corresponded to connecting to a directory server]
```

Tor is making its first connection to a relay. This might be through
a pluggable transport or proxy connection that Tor has already
established.

```text
  Phase 10:
  tag=conn_done summary="Connected to a relay"
 [New in 0.4.0.x]

  Tor has completed its first connection to a relay.

  Phase 14:
  tag=handshake summary="Handshaking with a relay"
 [New in 0.4.0.x; prior versions of Tor had a "handshake_dir" phase]

  Tor is in the process of doing a TLS handshake with a relay.

  Phase 15:
  tag=handshake_done summary="Handshake with a relay done"
 [New in 0.4.0.x]

  Tor has completed its TLS handshake with a relay.
```

<a id="control-spec.txt-5.5.3"></a>

### Phases in Bootstrap Stage 2  { #bootstrap-stage2 }

```text
  Phase 20:
  tag=onehop_create summary="Establishing an encrypted directory connection"
 [prior to 0.4.0.x, this was numbered 15]
```

Once TLS is finished with a relay, Tor will send a CREATE_FAST cell
to establish a one-hop circuit for retrieving directory information.
It will remain in this phase until it receives the CREATED_FAST cell
back, indicating that the circuit is ready.

```text
  Phase 25:
  tag=requesting_status summary="Asking for networkstatus consensus"
 [prior to 0.4.0.x, this was numbered 20]
```

Once we've finished our one-hop circuit, we will start a new stream
for fetching the networkstatus consensus. We'll stay in this phase
until we get the 'connected' relay cell back, indicating that we've
established a directory connection.

```text
  Phase 30:
  tag=loading_status summary="Loading networkstatus consensus"
 [prior to 0.4.0.x, this was numbered 25]
```

Once we've established a directory connection, we will start fetching
the networkstatus consensus document. This could take a while; this
phase is a good opportunity for using the "progress" keyword to indicate
partial progress.

This phase could stall if the directory server we picked doesn't
have a copy of the networkstatus consensus so we have to ask another,
or it does give us a copy but we don't find it valid.

Phase 40:
tag=loading_keys summary="Loading authority key certs"

Sometimes when we've finished loading the networkstatus consensus,
we find that we don't have all the authority key certificates for the
keys that signed the consensus. At that point we put the consensus we
fetched on hold and fetch the keys so we can verify the signatures.

Phase 45
tag=requesting_descriptors summary="Asking for relay descriptors"

Once we have a valid networkstatus consensus and we've checked all
its signatures, we start asking for relay descriptors. We stay in this
phase until we have received a 'connected' relay cell in response to
a request for descriptors.

```text
 [Some versions of Tor (starting with 0.2.6.2-alpha but before
  0.4.0.x): Tor could report having internal paths only; see Section
  5.6]
```

Phase 50:
tag=loading_descriptors summary="Loading relay descriptors"

We will ask for relay descriptors from several different locations,
so this step will probably make up the bulk of the bootstrapping,
especially for users with slow connections. We stay in this phase until
we have descriptors for a significant fraction of the usable relays
listed in the networkstatus consensus (this can be between 25% and 95%
depending on Tor's configuration and network consensus parameters).
This phase is also a good opportunity to use the "progress" keyword to
indicate partial steps.

```text
 [Some versions of Tor (starting with 0.2.6.2-alpha but before
  0.4.0.x): Tor could report having internal paths only; see Section
  5.6]

  Phase 75:
  tag=enough_dirinfo summary="Loaded enough directory info to build
  circuits"
 [New in 0.4.0.x; previously, Tor would misleadingly report the
  "conn_or" tag once it had enough directory info.]
```

<a id="control-spec.txt-5.5.4"></a>

### Phases in Bootstrap Stage 3  { #bootstrap-stage3 }

```text
  Phase 76:
  tag=ap_conn_pt summary="Connecting to pluggable transport to build
  circuits"
 [New in 0.4.0.x]
```

This is similar to conn_pt, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 77:
  tag=ap_conn_done_pt summary="Connected to pluggable transport to build circuits"
 [New in 0.4.0.x]
```

This is similar to conn_done_pt, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 78:
  tag=ap_conn_proxy summary="Connecting to proxy to build circuits"
 [New in 0.4.0.x]
```

This is similar to conn_proxy, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 79:
  tag=ap_conn_done_proxy summary="Connected to proxy to build circuits"
 [New in 0.4.0.x]
```

This is similar to conn_done_proxy, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 80:
  tag=ap_conn summary="Connecting to a relay to build circuits"
 [New in 0.4.0.x]
```

This is similar to conn, except for making connections to additional
relays or bridges that Tor needs to use to build application
circuits.

```text
  Phase 85:
  tag=ap_conn_done summary="Connected to a relay to build circuits"
 [New in 0.4.0.x]
```

This is similar to conn_done, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 89:
  tag=ap_handshake summary="Finishing handshake with a relay to build circuits"
 [New in 0.4.0.x]
```

This is similar to handshake, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 90:
  tag=ap_handshake_done summary="Handshake finished with a relay to build circuits"
 [New in 0.4.0.x]
```

This is similar to handshake_done, except for making connections to
additional relays or bridges that Tor needs to use to build
application circuits.

```text
  Phase 95:
  tag=circuit_create summary="Establishing a[n internal] Tor circuit"
 [prior to 0.4.0.x, this was numbered 90]
```

Once we've finished our TLS handshake with the first hop of a circuit,
we will set about trying to make some 3-hop circuits in case we need them
soon.

```text
 [Some versions of Tor (starting with 0.2.6.2-alpha but before
  0.4.0.x): Tor could report having internal paths only; see Section
  5.6]
```

Phase 100:
tag=done summary="Done"

A full 3-hop circuit has been established. Tor is ready to handle
application connections now.

```text
 [Some versions of Tor (starting with 0.2.6.2-alpha but before
  0.4.0.x): Tor could report having internal paths only; see Section
  5.6]
```

<a id="control-spec.txt-5.6"></a>

## Bootstrap phases reported by older versions of Tor { #bootstrap-obsolete }

These phases were reported by Tor older than 0.4.0.x.  For newer
versions of Tor, see Section 5.5.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor will build both
  exit and internal circuits. When bootstrap completes, Tor will be ready
  to handle an application requesting an exit circuit to services like the
  World Wide Web.
```

If the consensus does not contain Exits, Tor will only build internal
circuits. In this case, earlier statuses will have included "internal"
as indicated above. When bootstrap completes, Tor will be ready to handle
an application requesting an internal circuit to hidden services at
".onion" addresses.

If a future consensus contains Exits, exit circuits may become available.\]

Phase 0:
tag=starting summary="Starting"

Tor starts out in this phase.

Phase 5:
tag=conn_dir summary="Connecting to directory server"

Tor sends this event as soon as Tor has chosen a directory server --
e.g. one of the authorities if bootstrapping for the first time or
after a long downtime, or one of the relays listed in its cached
directory information otherwise.

Tor will stay at this phase until it has successfully established
a TCP connection with some directory server. Problems in this phase
generally happen because Tor doesn't have a network connection, or
because the local firewall is dropping SYN packets.

Phase 10:
tag=handshake_dir summary="Finishing handshake with directory server"

This event occurs when Tor establishes a TCP connection with a relay or
authority used as a directory server (or its https proxy if it's using
one). Tor remains in this phase until the TLS handshake with the relay
or authority is finished.

Problems in this phase generally happen because Tor's firewall is
doing more sophisticated MITM attacks on it, or doing packet-level
keyword recognition of Tor's handshake.

Phase 15:
tag=onehop_create summary="Establishing an encrypted directory connection"

Once TLS is finished with a relay, Tor will send a CREATE_FAST cell
to establish a one-hop circuit for retrieving directory information.
It will remain in this phase until it receives the CREATED_FAST cell
back, indicating that the circuit is ready.

Phase 20:
tag=requesting_status summary="Asking for networkstatus consensus"

Once we've finished our one-hop circuit, we will start a new stream
for fetching the networkstatus consensus. We'll stay in this phase
until we get the 'connected' relay cell back, indicating that we've
established a directory connection.

Phase 25:
tag=loading_status summary="Loading networkstatus consensus"

Once we've established a directory connection, we will start fetching
the networkstatus consensus document. This could take a while; this
phase is a good opportunity for using the "progress" keyword to indicate
partial progress.

This phase could stall if the directory server we picked doesn't
have a copy of the networkstatus consensus so we have to ask another,
or it does give us a copy but we don't find it valid.

Phase 40:
tag=loading_keys summary="Loading authority key certs"

Sometimes when we've finished loading the networkstatus consensus,
we find that we don't have all the authority key certificates for the
keys that signed the consensus. At that point we put the consensus we
fetched on hold and fetch the keys so we can verify the signatures.

```text
  Phase 45
  tag=requesting_descriptors summary="Asking for relay descriptors
                                      [ for internal paths]"
```

Once we have a valid networkstatus consensus and we've checked all
its signatures, we start asking for relay descriptors. We stay in this
phase until we have received a 'connected' relay cell in response to
a request for descriptors.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor will ask for
  descriptors for both exit and internal paths. If not, Tor will only ask
  for descriptors for internal paths. In this case, this status will
  include "internal" as indicated above.]

  Phase 50:
  tag=loading_descriptors summary="Loading relay descriptors[ for internal
                                   paths]"
```

We will ask for relay descriptors from several different locations,
so this step will probably make up the bulk of the bootstrapping,
especially for users with slow connections. We stay in this phase until
we have descriptors for a significant fraction of the usable relays
listed in the networkstatus consensus (this can be between 25% and 95%
depending on Tor's configuration and network consensus parameters).
This phase is also a good opportunity to use the "progress" keyword to
indicate partial steps.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor will download
  descriptors for both exit and internal paths. If not, Tor will only
  download descriptors for internal paths. In this case, this status will
  include "internal" as indicated above.]
```

Phase 80:
tag=conn_or summary="Connecting to the Tor network\[ internally\]"

Once we have a valid consensus and enough relay descriptors, we choose
entry guard(s) and start trying to build some circuits. This step
is similar to the "conn_dir" phase above; the only difference is
the context.

If a Tor starts with enough recent cached directory information,
its first bootstrap status event will be for the conn_or phase.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor will build both
  exit and internal circuits. If not, Tor will only build internal circuits.
  In this case, this status will include "internal(ly)" as indicated above.]

  Phase 85:
  tag=handshake_or summary="Finishing handshake with first hop[ of internal
                            circuit]"
```

This phase is similar to the "handshake_dir" phase, but it gets reached
if we finish a TCP connection to a Tor relay and we have already reached
the "conn_or" phase. We'll stay in this phase until we complete a TLS
handshake with a Tor relay.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor may be finishing
  a handshake with the first hop if either an exit or internal circuit. In
  this case, it won't specify which type. If the consensus contains no Exits,
  Tor will only build internal circuits. In this case, this status will
  include "internal" as indicated above.]
```

Phase 90:
tag=circuit_create summary="Establishing a\[n internal\] Tor circuit"

Once we've finished our TLS handshake with the first hop of a circuit,
we will set about trying to make some 3-hop circuits in case we need them
soon.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor will build both
  exit and internal circuits. If not, Tor will only build internal circuits.
  In this case, this status will include "internal" as indicated above.]
```

Phase 100:
tag=done summary="Done"

A full 3-hop circuit has been established. Tor is ready to handle
application connections now.

```text
 [Newer versions of Tor (0.2.6.2-alpha and later):
  If the consensus contains Exits (the typical case), Tor will build both
  exit and internal circuits. At this stage, Tor will be ready to handle
  an application requesting an exit circuit to services like the World
  Wide Web.
```

If the consensus does not contain Exits, Tor will only build internal
circuits. In this case, earlier statuses will have included "internal"
as indicated above. At this stage, Tor will be ready to handle an
application requesting an internal circuit to hidden services at ".onion"
addresses.

If a future consensus contains Exits, exit circuits may become available.\]
