# TC: A Tor control protocol (Version 1)

<a id="control-spec.txt-0"></a>

## Scope

This document describes an implementation-specific protocol that is used
for other programs (such as frontend user-interfaces) to communicate with a
locally running Tor process.  It is not part of the Tor onion routing
protocol.

This protocol replaces version 0 of TC, which is now deprecated.  For
reference, TC is described in "control-spec-v0.txt".  Implementors are
recommended to avoid using TC directly, but instead to use a library that
can easily be updated to use the newer protocol.  (Version 0 is used by Tor
versions 0.1.0.x; the protocol in this document only works with Tor
versions in the 0.1.1.x series and later.)

```text
      The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
      NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
      "OPTIONAL" in this document are to be interpreted as described in
      RFC 2119.
```
