<a id="bandwidth-file-spec.txt-1"></a>

# Scope and preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
RFC 2119.

<a id="bandwidth-file-spec.txt-1.2"></a>

## Acknowledgements

The original bandwidth generator (Torflow) and format was
created by mike. Teor suggested to write this specification while
contributing on pastly's new bandwidth generator implementation.

This specification was revised after feedback from:

Nick Mathewson (nickm)
Iain Learmonth (irl)

<a id="bandwidth-file-spec.txt-1.3"></a>

## Outline

The Tor directory protocol (dir-spec.txt \[3\]) sections 3.4.1
and 3.4.2, use the term bandwidth measurements, to refer to what
here is called Bandwidth File.

A Bandwidth File contains information on relays' bandwidth
capacities and is produced by bandwidth generators, previously known
as bandwidth scanners.

<a id="bandwidth-file-spec.txt-1.4"></a>

## Format Versions

1.0.0 - The legacy Bandwidth File format

```text
  1.1.0 - Adds a header containing information about the bandwidth
          file. Document the sbws and Torflow relay line keys.

  1.2.0 - If there are not enough eligible relays, the bandwidth file
          SHOULD contain a header, but no relays. (To match Torflow's
          existing behaviour.)

          Adds scanner and destination countries to the header.
          Adds new KeyValue Lines to the Header List section with
          statistics about the number of relays included in the file.
          Adds new KeyValues to Relay Bandwidth Lines, with different
          bandwidth values (averages and descriptor bandwidths).

  1.4.0 - Adds monitoring KeyValues to the header and relay lines.

          RelayLines for excluded relays MAY be present in the bandwidth
          file for diagnostic reasons. Similarly, if there are not enough
          eligible relays, the bandwidth file MAY contain all known relays.

          Diagnostic relay lines SHOULD be marked with vote=0, and
          Tor SHOULD NOT use their bandwidths in its votes.

          Also adds Tor version.
  1.5.0 - Removes "recent_measurement_attempt_count" KeyValue.
  1.6.0 - Adds congestion control stream events KeyValues.
  1.7.0 - Adds ratios KeyValues to the relay lines and network averages
          KeyValues to the header.

  All Tor versions can consume format version 1.0.0.
```

All Tor versions can consume format version 1.1.0 and later,
but Tor versions earlier than 0.3.5.1-alpha warn if the header
contains any KeyValue lines after the Timestamp.

```text
  Tor versions 0.4.0.3-alpha, 0.3.5.8, 0.3.4.11, and earlier do not
  understand "vote=0". Instead, they will vote for the actual bandwidths
  that sbws puts in diagnostic relay lines:
    * 1 for relays with "unmeasured=1", and
    * the relay's measured and scaled bandwidth when "under_min_report=1".
```
