<a id="guard-spec.txt-TODO"></a>

# Still non-addressed issues \[Section:TODO\]

Simulate to answer:  Will this work in a dystopic world?

Simulate actual behavior.

For all lifetimes: instead of storing the "this began at" time,
store the "remove this at" time, slightly randomized.

Clarify that when you get a `<complete>` circuit, you might need to
relaunch circuits through that same guard immediately, if they
are circuits that have to be independent.

Fix all items marked XX or TODO.

"Directory guards" -- do they matter?

```text
       Suggestion: require that all guards support downloads via BEGINDIR.
       We don't need to worry about directory guards for relays, since we
       aren't trying to prevent relay enumeration.

   IP version preferences via ClientPreferIPv6ORPort

       Suggestion: Treat it as a preference when adding to
       {CONFIRMED_GUARDS}, but not otherwise.
```
